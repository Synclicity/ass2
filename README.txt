VRENT SYSTEM 1.0 DESIGNED SOLELY FOR MR NEFOS
JAVA VERSION: JDK7

Features:

- Command Line User Interface
- Inventory Control
- Membership Control
- Rental Processing
- Searching and displaying

How to use:

- Import as Netbeans/blueJ project or otherwise
- Compile and run
- Follow the user interface (Enter numbers to select an option)

Terms of Use:

- ABSOLUTELY NO TERMS, DO WHATEVER YOU WANT
- I AM NOT RESPONSIBLE FOR ANY FAILURE OF THE SYSTEM UNLESS YOU HAVE A COMMERCIAL LICENCE
- COMMERCIAL LICENCE CAN BE PURCHASED FOR $10000