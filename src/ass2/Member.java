/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass2;

/**
 *
 * @author Tony
 */
public class Member implements java.io.Serializable {

    private String contactNo;
    private String name;
    private String type;
    private String add;
    private String memNumber;
    private Privilege p;

    public Member() {

    }

    public Member(String contactNo, String name, String type) {
        this.contactNo = contactNo;
        this.name = name;
        this.type = type;
        this.p = new Privilege(type);
    }
    
    

    public String getMemNumber() {
        return memNumber;
    }

    public void setMemNumber(String memNumber) {
        this.memNumber = memNumber;
    }
    
    public int getMaxPoints() {
        if (this.type.equals("P")) {
            return 4;
        } else if (this.type.equals("B")) {
            return 2;
        } else {
            return 0;
        }
    }
    
    public double getDVDrentFee() {
        return p.getDVDrentFee();
    }
    
    public double getVCDrentFee() {
        return p.getVCDrentFee();
    }
    
    public double getDVDrenewFee() {
        return p.getDVDRenewFee();
    }
    
    public double getVCDrenewFee() {
        return p.getVCDRenewFee();        
    }
    
    public double getDVDlateFee() {
        return p.getLateFee();        
    }
    
    public double getVCDlateFee() {
        return p.getLateFee();  
    }

    public String getAdd() {
        return add;
    }

    public void setAdd(String add) {
        this.add = add;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        this.p = new Privilege(type);
    }

    
}
