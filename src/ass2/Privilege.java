/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass2;

/**
 *
 * @author Tony
 */
public class Privilege implements java.io.Serializable {

    private String type;
    private double DVDrentFee = 0.00;
    private double VCDrentFee = 0.00;

    public Privilege(String type) {
        this.type = type;
        if (type.equals("B")) {
            this.DVDrentFee = 5.00;
            this.VCDrentFee = 2.50;
        } else {
            this.DVDrentFee = 3.00;
            this.VCDrentFee = 1.00;
        }
    }

    public String getType() {
        return this.type;
    }

    public double getDVDrentFee() {
        return this.DVDrentFee;
    }

    public double getVCDrentFee() {
        return this.VCDrentFee;
    }
    
    public double getDVDRenewFee() {
        return 0.50;
    }
    
    public double getVCDRenewFee() {
        return 1.00;
    }
    
    public double getLateFee() {
        return 10.00;
    }
    
    public double getDVDReplacementFee() {
        return 30.00;
    }
    
    public double getVCDReplacementFee() {
        return 15.00;
    }

}
