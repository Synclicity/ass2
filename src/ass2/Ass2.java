package ass2;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.Date;

public class Ass2 {

    private static Scanner reader = new Scanner(System.in);
    private static MovieSystem m = new MovieSystem();

    public static void main(String[] args) {
        //First we create the directories and preload if it's first run
        preload_data();

        //Main UI
        main_menu();
    }

    private static void main_menu() {
        System.out.println(">>Main Menu<<\n"
                + "1. Stock Control\n"
                + "2. Membership Record\n"
                + "3. Rental\n"
                + "4. Return Movie\n"
                + "5. Search Movie\n"
                + "0. Exit\n"
                + "\n Enter option:");

        int option = Integer.parseInt(reader.nextLine());

        switch (option) {
            case 0:
                System.exit(0);
                break;
            case 1:
                stock_control();
                break;
            case 2:
                membership_record_menu();
                break;
            case 3:
                rental_menu();
                break;
            case 4:
                return_movie_menu();
                break;
            case 5:
                search_movie_menu();
                break;
            default:
                System.exit(0);
        }
    }

    private static void stock_control() {

        System.out.println(">>Main Menu<<\n\n"
                + "1. Add Movie\n"
                + "2. Display All Movies\n"
                + "0. Main Menu\n"
                + "\nEnter option:");

        int option = Integer.parseInt(reader.nextLine());;

        switch (option) {
            case 0:
                main_menu();
                break;
            case 1:
                add_movie_menu();
                break;
            case 2:
                display_all_movies();
                break;
            default:
                main_menu();
        }
    }

    private static void membership_record_menu() {
        System.out.println(">>Membership Record<<\n\n"
                + "1. Register Member\n"
                + "2. Delete Member\n"
                + "3. Upgrade member\n"
                + "4. View Member List\n"
                + "5. Query Member Details\n"
                + "0. Main Menu\n\nEnter option:");
        int option = Integer.parseInt(reader.nextLine());
        switch (option) {
            case 1:
                register_member_menu();
                break;
            case 2:
                delete_member_menu();
                break;
            case 3:
                upgrade_member_menu();
                break;
            case 4:
                view_member_list_menu();
                break;
            case 5:
                query_member_details_menu();
                break;
            default:
                main_menu();
                break;
        }
    }

    private static void add_movie_menu() {

        System.out.println(">>Add Movie<<\n\nTitle:");
        String title = reader.nextLine();

        System.out.println("Year:");
        int year = Integer.parseInt(reader.nextLine());;

        System.out.println("Director:");
        String director = reader.nextLine();

        System.out.println("Rating [G / PG / M / MA15 / R18]:");
        String rating = reader.nextLine();

        System.out.println("Genre [a - Action/ b - Drama/ c - Comedy/ d - "
                + "Musical/ e - Family/ f - Documentary]:");
        String genre = reader.nextLine();

        System.out.println("Format [VCD/DVD]:");
        String format = reader.nextLine();

        System.out.println("Cost:");
        double cost = Double.parseDouble(reader.nextLine());

        System.out.println("Quantity:");
        int quantity = Integer.parseInt(reader.nextLine());;

        m.displayNewMovie(title, year, director, rating, genre,
                format, cost, quantity);
        System.out.println("Confirm?\n1. Yes  2. No, return to main menu");
        int option = Integer.parseInt(reader.nextLine());

        if (option == 1) {

            //Handle special case
            if (m.movieExists(title)) {
                System.out.println("Movie already exists! Add copies?\n"
                        + "1. Yes  2. No, return to main menu");
                option = Integer.parseInt(reader.nextLine());

                if (option != 1) {
                    main_menu();
                }

            } else {
                m.createMovie(title, year, director, rating, genre);
            }

            //Print out new movie details
            int c = 0;
            int id = m.getNewestCopyID(title);
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

            while (c < quantity) {

                //Add a copy of the given movie
                m.addCopy(title, format, cost);
                System.out.println("Copy ID:" + id
                        + ", Cost:" + cost
                        + ", Format:" + format
                        + ", Date Created:" + dateFormat.format(date));
                c++;
                id++;
            }

            System.out.println("Saving Performed\n"
                    + "Movie(s) is added to store\n");
            get_enter_input();

        } else {
            main_menu();
        }

    }

    private static void display_all_movies() {
        m.displayAllMovies();
        get_enter_input();
    }

    private static void get_enter_input() {
        System.out.println("Hit Enter to return to Main menu\n");
        //Wait for user input
        reader.nextLine();

        main_menu();
    }

    private static void register_member_menu() {
        System.out.println(">>Register Member<<\n\n"
                + "Enter details of new member:\n"
                + "Name:");
        String name = reader.nextLine();
        System.out.println("Member Number:");
        String memNum = reader.nextLine();
        System.out.println("Address:");
        String add = reader.nextLine();
        System.out.println("Contact No:");
        String contactNo = reader.nextLine();
        System.out.println("Type (B/P):");
        String type = reader.nextLine();

        //This creates the member AND displays all member details
        m.createMember(contactNo, name, type, add, memNum);

        get_enter_input();

    }

    private static void delete_member_menu() {
        System.out.println("\n>>Delete Member<<\n\n"
                + "Enter Member Number:");
        String memNumber = reader.nextLine();

        m.cancelMember(memNumber);

        get_enter_input();
    }

    private static void upgrade_member_menu() {
        System.out.println("\n>>Upgrade Member<<\n\n"
                + "Enter Member Number to upgrade:");
        String memNumber = reader.nextLine();
        m.upgradeMember(memNumber);

        get_enter_input();

    }

    private static void view_member_list_menu() {
        System.out.println("\n>>View Member List<<\n\n");
        m.displayAllMembers();

        get_enter_input();
    }

    private static void query_member_details_menu() {
        System.out.println(">>Query Member<<\n\n"
                + "Enter Member Number:");
        String memNumber = reader.nextLine();
        m.displayMember(memNumber);
        get_enter_input();
    }

    private static void preload_data() {
        //Make the resources directory if it doesn't exist
        File file = new File("./resources/");
        if (!file.exists()) {
            file.mkdir();

            //Make subdirectories
            file = new File("./resources/movies/");
            file.mkdir();

            file = new File("./resources/copies/");
            file.mkdir();

            file = new File("./resources/members/");
            file.mkdir();

            file = new File("./resources/rentals/");
            file.mkdir();

            //Preload basic and premium privileges
            //Preload Movie/Copy data
            m.createMovie("Superman Returns", 2015, "Ben Airy", "M", "A");
            int c = 0;
            for (c = 0; c < 10; c++) {
                m.addCopy("Superman Returns", "DVD", 39.99);
            }
            m.createMovie("Ant-man", 2015, "Steven Young", "PG", "A");
            for (c = 0; c < 12; c++) {
                m.addCopy("Ant-man", "VCD", 19.99);
            }

            //Preload Members
            m.silent_createMember("042316478", "Ben Choi", "B",
                    "UNSW Campus", "001");
            m.silent_createMember("04331122", "Mickey Mouse", "P",
                    "123 Anzac Pde, Sydney", "002");
        }

    }

    private static void rental_menu() {
        System.out.println("\n>>Movie Rental<<\n\n"
                + "Enter Member Number:");
        String memNumber = reader.nextLine();
        String s = "";
        int flag = 1;
        Double cost = 0.00;
        while (flag == 1) {
            System.out.println("\nEnter Movie Title:");
            String title = reader.nextLine();

            System.out.println("Enter Movie Format:");
            String format = reader.nextLine();

            //Store details of rented items
            s = s.concat(m.makeRental(title, format, memNumber));

            //Calculate cost of rented items
            cost += m.costOfRent(memNumber, format);

            System.out.println("Saving performed\n"
                    + "Continue to rent more movies?\n"
                    + "1. Yes  2. No");
            flag = Integer.parseInt(reader.nextLine());

        }
        System.out.println("<<<Printing rented items>>>\n"
                + "Amount payable: " + cost + "\n\n" + s
                + "\nSaving Performed");

        get_enter_input();

    }

    private static void return_movie_menu() {
        System.out.println(">>Return Movie<<\n\n"
                + "Enter Member Number:");
        String memNumber = reader.nextLine();
        double fines = 0.00;
        int flag = 1;
        while (flag == 1) {
            System.out.println("Enter movie title to return:");
            String title = reader.nextLine();
            System.out.println("Enter movie format [VCD/DVD]:");
            String format = reader.nextLine();
            System.out.println("\nConfirm?\n"
                    + "1. Yes  2. No, return to Main Menu\n"
                    + "Enter option:");
            int option = Integer.parseInt(reader.nextLine());

            if (option == 2) {
                main_menu();
            }

            fines += m.collectMovie(title, format, memNumber);

            System.out.println("\nAny more movies to return?\n"
                    + "1. Yes  2. No\n"
                    + "Enter option:");
            flag = Integer.parseInt(reader.nextLine());
        }

        System.out.println("\nFines incurred: $" + fines + "\n");

        //Exceptional case
        if (fines > 0) {
            System.out.println("Payment collected?\n"
                    + "1. Yes  2. No, return to Main Menu\n"
                    + "Enter option:");
            if (reader.nextLine().equals("2")) {
                main_menu();
            }
        }

        System.out.println("Saving Performed");
        get_enter_input();

    }

    private static void search_movie_menu() {
        System.out.println(">>Search Movie<<\n\n"
                + "Search criteria\n\n"
                + "1. Title\n"
                + "2. Year\n"
                + "3. Director\n"
                + "4. Rating\n"
                + "5. Genre\n"
                + "0. Main Menu\n\n"
                + "Enter option:");
        int option = Integer.parseInt(reader.nextLine());
        switch (option) {
            case 1:
                search_title();
                break;
            case 2:
                search_year();
                break;
            case 3:
                search_director();
                break;
            case 4:
                search_rating();
                break;
            case 5:
                search_genre();
                break;
            default:
                main_menu();
                break;
        }
    }

    private static void search_title() {
        System.out.println("Enter title to be searched:");
        String title = reader.nextLine();
        m.search_movie_title(title);
        get_enter_input();
    }

    private static void search_year() {
        System.out.println("Enter year to be searched:");
        String year = reader.nextLine();
        m.search_movie_year(year);
        get_enter_input();
    }

    private static void search_director() {
        System.out.println("Enter director to be searched:");
        String director = reader.nextLine();
        m.search_movie_director(director);
        get_enter_input();
    }

    private static void search_genre() {
        System.out.println("Enter search_genre to be searched:");
        String search_genre = reader.nextLine();
        m.search_movie_genre(search_genre);
        get_enter_input();
    }

    private static void search_rating() {
        System.out.println("Enter rating to be searched:");
        String rating = reader.nextLine();
        m.search_movie_rating(rating);
        get_enter_input();
    }
}
