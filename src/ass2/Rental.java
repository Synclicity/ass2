/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ass2;

import java.util.Date;
import java.util.Calendar;

/**
 *
 * @author Tony
 */
public class Rental implements java.io.Serializable {

    private Date dateRented;
    private Date dateToReturn;
    private String memNumber;
    private String format;
    private String title;
    private String id;

    public Rental() {
        Date date = new Date();
        dateRented = date;

        //Calendar Arithmetic is hard for some reason
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 4); //Add 4 days
        dateToReturn = cal.getTime();

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDateRented() {
        return dateRented;
    }

    public void setDateRented(Date dateRented) {
        this.dateRented = dateRented;
    }

    public Date getDateToReturn() {
        return dateToReturn;
    }

    public void setDateToReturn(Date dateToReturn) {
        this.dateToReturn = dateToReturn;
    }

    public String getMemNumber() {
        return memNumber;
    }

    public void setMemNumber(String memNumber) {
        this.memNumber = memNumber;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
