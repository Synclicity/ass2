package ass2;

public class MovieSystem {

    private static VideoShop v = VideoShop.getInstance();

    public void createMovie(String title, int year, String director,
            String rating, String genre) {

        v.createMovie(title, year, director, rating, genre);
    }

    public void addCopy(String title, String format, Double cost) {
        v.addCopy(title, format, cost);
    }

    public boolean movieExists(String title) {

        return v.movieExists(title);
    }

    public int getNewestCopyID(String title) {
        int newest = v.getNumCopies(title) + 1;
        return newest;
    }

    public void displayAllMovies() {
        v.displayAllMovies();
    }
    
    public void search_movie_title(String title) {
        v.displayMovieDetails(title);
    }
    
    public void search_movie_year(String year) {
        v.displaySearchResults(year, "year");
    }
    public void search_movie_director(String director) {
        v.displaySearchResults(director, "director");
    }
    public void search_movie_rating(String rating) {
        v.displaySearchResults(rating, "rating");
    }
    public void search_movie_genre(String genre) {
        v.displaySearchResults(genre, "genre");
    }

    public String makeRental(String movie, String format, String memNumber) {
        
        String s = v.makeRental(movie, format, memNumber);
        
        return s;
        
    }
    
    public double costOfRent(String memNumber, String format) {
        
        return v.getCostOfRent(memNumber, format);
    }

    public double collectMovie(String title, String format, String memNumber) {
        return v.collectMovie(title, format, memNumber);
    }

    public void displayMovieTitle() {

    }

    public void displayNewMovie(String title, int year, String director,
            String rating, String genre, String format, double cost,
            int quantity) {
        System.out.printf("\n\n\nDetails of new movie\n\n"
                + "Title:%s\n"
                + "Year:%d\n"
                + "Director:%s\n"
                + "Rating:%s"
                + "Genre:%s\n"
                + "Format:%s\n"
                + "Cost:%f\n"
                + "Quantity:%d\n", title, year, director, rating,
                genre, format, cost, quantity);
    }

    public void createMember(String contactNo, String name, String type,
            String add, String memNumber) {
        if (!v.memberExists(memNumber)) {
            v.createMember(contactNo, name, type, add, memNumber);
        } else {
            System.out.println("Member already exists.\n\n");
        }
        System.out.println(">>Displaying all members<<\n");
        displayAllMembers();
        System.out.println("\n\nSaving Performed\n");
    }
    
    public void silent_createMember(String contactNo, String name, String type,
            String add, String memNumber) {
        if (!v.memberExists(memNumber)) {
            v.createMember(contactNo, name, type, add, memNumber);
        }
    }

    public void displayAllMembers() {
        v.displayAllMembers();
    }

    public void upgradeMember(String memNumber) {
        if (!v.memberExists(memNumber)) {
            System.out.println("\nMember not found\n");
        } else {
            v.upgradeMember(memNumber);
            System.out.println("\nSaving Performed\n");
        }
    }

    public void cancelMember(String memNumber) {
        if (!v.memberExists(memNumber)) {
            System.out.println("\nMember not found\n");
        } else {
            v.cancelMember(memNumber);
            System.out.println("\nSaving Performed\nMember is deleted\n");
        }

    }

    public void displayMember(String memNumber) {
        v.displayMember(memNumber);
        
    }
    
    

}
