package ass2;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class VideoShop {

    private static VideoShop theOne;

    private VideoShop() {

    }

    public static VideoShop getInstance() {
        if (theOne == null) {
            theOne = new VideoShop();
        }
        return theOne;
    }

    public boolean movieExists(String title) {
        String path = "./resources/movies/" + title + ".ser";
        File file = new File(path);
        if (file.exists()) {
            return true;
        } else {
            return false;
        }
    }

    public void createMovie(String title, int year, String director,
            String rating, String genre) {

        Movie m = getMovie();
        m.setTitle(title);
        m.setYear(year);
        m.setDirector(director);
        m.setRating(rating);
        m.setGenre(genre);

        //Write the new movie object m using java serialisation
        try {
            //Make the file

            //Create output streams
            FileOutputStream fileOut
                    = new FileOutputStream("./resources/movies/" + title + ".ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(m);
            out.close();
            fileOut.close();

        } catch (IOException i) {
            //Error handling
            i.printStackTrace();
        }
    }

    public String getMovieID(String title) {
        String id = "-1";
        int idInt = 0;
        //Some quick hax for movie ID
        //Iterate through movies directory
        String path = ".\\resources\\movies\\" + title + ".ser";
        String dir = "./resources/movies/";
        File file = new File(dir);
        File[] allMovies = file.listFiles();
        for (File f : allMovies) {
            idInt++;
            if (f.getPath().equals(path)) {
                id = Integer.toString(idInt);
                break;
            }
        }
        return id;
    }

    public void displayAllMovies() {
        //Iterate through movies directory
        String dir = "./resources/movies/";
        File file = new File(dir);
        File[] allMovies = file.listFiles();
        Movie m = null;
        String id = "2"; //The spec starts at 2 lol
        if (allMovies != null) {
            for (File f : allMovies) {
                //Convert f to object
                try {
                    FileInputStream fileIn = new FileInputStream(f);
                    ObjectInputStream in = new ObjectInputStream(fileIn);
                    m = (Movie) in.readObject();

                    //Print out relevant information
                    System.out.println("ID:" + getMovieID(m.getTitle())
                            + ", Title: " + m.getTitle()
                            + ", Year: " + m.getYear() + ", Director: "
                            + m.getDirector() + ", Rating: " + m.getRating()
                            + ", Genre: " + m.getGenre().toUpperCase());

                    in.close();
                    fileIn.close();
                } catch (IOException i) {
                    i.printStackTrace();
                } catch (ClassNotFoundException c) {
                    System.out.println("Movie class not found");
                    c.printStackTrace();
                }
            }
        }
    }

    //Method to number of copies of a movie
    public int getNumCopies(String title) {
        String dir = "./resources/copies/" + title + "/";
        File file = new File(dir);
        int num = 0;

        //If the movie doesn't exist yet
        if (!file.exists()) {
            return 0;
        }

        File[] allCopies = file.listFiles();
        if (allCopies != null) {
            //Count the number of files the dumb way
            for (File f : allCopies) {
                num++;
            }
        }

        return num;
    }

    public int getAvailableCopies(String title) {
        String dir = "./resources/copies/" + title + "/";
        File file = new File(dir);
        int num = 0;

        //If the movie doesn't exist yet
        if (!file.exists()) {
            return 0;
        }

        //Iterate through every .ser object and check available
        File[] allCopies = file.listFiles();
        Copy copy = null;
        if (allCopies != null) {
            for (File f : allCopies) {
                //Convert f to object
                try {
                    FileInputStream fileIn = new FileInputStream(f);
                    ObjectInputStream in = new ObjectInputStream(fileIn);
                    copy = (Copy) in.readObject();

                    //Check status
                    if (copy.getStatus() == 'a') {
                        num++;
                    }

                    in.close();
                    fileIn.close();
                } catch (IOException i) {
                    i.printStackTrace();
                } catch (ClassNotFoundException c) {
                    System.out.println("Movie class not found");
                    c.printStackTrace();
                }
            }
        }

        return num;
    }

    public void addCopy(String title, String format, Double cost) {

        int id = getNumCopies(title) + 1;

        Copy c = getCopy();
        c.setTitle(title);
        c.setFormat(format);
        c.setCost(cost);
        c.setID(id);

        //Store this copy using java serialisation
        String dir = "./resources/copies/" + title + "/";
        //Make the folder for the movie first if it doesn't exist
        File file = new File(dir);
        if (!file.exists()) {
            file.mkdir();
        }
        try {

            //Create output streams
            FileOutputStream fileOut
                    = new FileOutputStream(dir + "/" + id + ".ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(c);
            out.close();
            fileOut.close();

        } catch (IOException i) {
            //Error handling
            i.printStackTrace();
        }

    }

    //Returns copy ID
    //Switches status to borrowed (b)
    public String removeCopy(String title, String format) {
        String id = "-1";

        //Iterate through the copies folder
        //Find first match that has status 'a' and format specified
        String folder = "./resources/copies/" + title + "/";
        File file = new File(folder);
        //Iterate through every .ser object and check available
        File[] allCopies = file.listFiles();
        Copy copy = null;

        if (allCopies != null) {
            for (File f : allCopies) {
                //Convert f to object
                try {
                    FileInputStream fileIn = new FileInputStream(f);
                    ObjectInputStream in = new ObjectInputStream(fileIn);
                    copy = (Copy) in.readObject();

                    //Check status
                    if (copy.getStatus() == 'a'
                            && copy.getFormat().equals(format)) {
                        //Convert file to id
                        id = f.getPath();
                        id = id.replace("\\", "/");
                        String[] parts = id.split("/");
                        id = parts[parts.length - 1];
                        id = id.replace(".ser", "");
                    }

                    in.close();
                    fileIn.close();

                    //Check whether we have changed the id
                    if (!id.equals("-1")) {

                        //Rewrite the object
                        copy.setStatus('b');
                        //Create output stream and rewrite
                        FileOutputStream fileOut
                                = new FileOutputStream(f);
                        ObjectOutputStream out
                                = new ObjectOutputStream(fileOut);
                        out.writeObject(copy);
                        out.close();
                        fileOut.close();

                        //Skip out of the loop
                        break;
                    }
                } catch (IOException i) {
                    i.printStackTrace();
                } catch (ClassNotFoundException c) {
                    System.out.println("Movie class not found");
                    c.printStackTrace();
                }
            }
        }

        return id;
    }

    public int getCurrentPoints(String memNumber) {
        //Iterate through every rental
        String path = "./resources/rentals/" + memNumber + "/";
        File file = new File(path);
        //Iterate through every .ser object and check available
        File[] allRentals = file.listFiles();
        Rental r = null;
        int total = 0;
        if (allRentals != null) {
            for (File f : allRentals) {
                //Convert f to object
                try {
                    FileInputStream fileIn = new FileInputStream(f);
                    ObjectInputStream in = new ObjectInputStream(fileIn);
                    r = (Rental) in.readObject();
                    in.close();
                    fileIn.close();

                    //Check format and add appropriate number of points
                    if (r.getFormat().equals("DVD")) {
                        total = total + 2;
                    } else if (r.getFormat().equals("VCD")) {
                        total++;
                    }
                } catch (IOException i) {
                    i.printStackTrace();
                } catch (ClassNotFoundException c) {
                    System.out.println("Movie class not found");
                    c.printStackTrace();
                }
            }
        }

        return total;
    }

    public String makeRental(String title, String format, String memNumber) {

        //Check if member exists
        if (!memberExists(memNumber)) {
            System.out.println("Member does not exist!\n");
            return "";
        }
        //Check if the member is allowed to rent
        Member m = getMemberFromNumber(memNumber);
        int newPoints = 0;
        if (format.equals("DVD")) {
            newPoints = 2;
        } else if (format.equals("VCD")) {
            newPoints = 1;
        }
        if ((getCurrentPoints(memNumber) + newPoints) > m.getMaxPoints()) {
            System.out.println("Member has exceeded their max quota.\n");
            return "Member must return some movies";
        }
        //Calculate the filename (format: title copyID)
        //First we find a valid copy else we return with error msg
        String id = removeCopy(title, format);

        if (id.equals("-1")) {
            System.out.println("Unavailable Movie!\n");
            return "";
        }
        Rental r = getRental();
        r.setTitle(title);
        r.setFormat(format);
        r.setMemNumber(memNumber);
        r.setId(id);

        //Find member's rental folder, add the rental object
        String path = "./resources/rentals/" + memNumber + "/"
                + title + "-" + id + ".ser";
        try {

            //Create output streams
            FileOutputStream fileOut
                    = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(r);
            out.close();
            fileOut.close();

        } catch (IOException i) {
            //Error handling
            i.printStackTrace();
        }

        //String to print rented items
        String mID = getMovieID(title);
        Movie movie = getMovieFromTitle(title);

        return "Date Rented: " + dateToString(r.getDateRented())
                + ", Date To Return: " + dateToString(r.getDateToReturn())
                + ", Movie ID: " + mID + ", Title: " + title + ", Year: "
                + movie.getYear() + ", Director: " + movie.getDirector()
                + ", Rating: " + movie.getDirector() + ", Genre: "
                + movie.getGenre() + "\n";

    }

    //Returns fines
    public double collectMovie(String title, String format, String memNumber) {

        Rental r = returnRental(title, format, memNumber);
        returnCopy(r);

        //Calculate fines
        double fines = calculateFines(r);

        return fines;
    }

    public double calculateFines(Rental r) {
        Member m = getMemberFromNumber(r.getMemNumber());
        //Calculate difference between return date and today
        long overdue = dateDiff(r.getDateToReturn(), new Date());
        String format = r.getFormat();
        if  (overdue <= 0) {
            return 0.00;
        } else if (overdue < 8) {
            if (format.equals("DVD")) {
                return overdue * m.getDVDrenewFee();
            } else if (format.equals("VCD")) {
                return overdue * m.getVCDrenewFee();
            } else {
                return 0.00;
            }
        } else {
            if (format.equals("DVD")) {
                return m.getDVDlateFee();
            } else if (format.equals("VCD")) {
                return m.getVCDlateFee();
            } else {
                return 0.00;
            }
        }
    }

    public long dateDiff(Date from, Date to) {
        long diff = to.getTime() - from.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public void returnCopy(Rental r) {
        String path = "./resources/copies/" + r.getTitle() + "/"
                + r.getId() + ".ser";
        File f = new File(path);
        Copy copy = null;
        try {
            //Retrieve the object
            FileInputStream fileIn = new FileInputStream(f);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            copy = (Copy) in.readObject();
            in.close();
            fileIn.close();

            //Rewrite the object
            copy.setStatus('a');
            //Create output stream and rewrite
            FileOutputStream fileOut
                    = new FileOutputStream(f);
            ObjectOutputStream out
                    = new ObjectOutputStream(fileOut);
            out.writeObject(copy);
            out.close();
            fileOut.close();

        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("Copy class not found");
            c.printStackTrace();
        }
    }

    public Rental returnRental(String title, String format, String memNumber) {
        Rental r = null;
        String dir = "./resources/rentals/" + memNumber + "/";
        File file = new File(dir);
        File[] allRentals = file.listFiles();
        if (allRentals != null) {
            for (File f : allRentals) {
                //Convert f to object
                try {
                    FileInputStream fileIn = new FileInputStream(f);
                    ObjectInputStream in = new ObjectInputStream(fileIn);
                    r = (Rental) in.readObject();

                    in.close();
                    fileIn.close();

                    //Matching with criteria
                    if (r.getTitle().equals(title)
                            && r.getFormat().equals(format)) {
                        f.delete();
                        return r;
                    }

                } catch (IOException i) {
                    i.printStackTrace();
                } catch (ClassNotFoundException c) {
                    System.out.println("Movie class not found");
                    c.printStackTrace();
                }
            }

        }
        //Keep compiler happy
        //Will still give nullPointerException error if rental doesn't exist
        return r;
    }

    public void displayMovieTitle(Movie m) {
        System.out.println(m.getTitle());
    }

    public void createMember(String contactNo, String name, String type,
            String add, String memNumber) {
        Member m = getMember();
        m.setAdd(add);
        m.setContactNo(contactNo);
        m.setName(name);
        m.setType(type);
        m.setMemNumber(memNumber);

        //Store m object using java serialisation
        String path = "./resources/members/" + memNumber + ".ser";
        //Create output streams
        try {
            //Make the file

            //Create output streams
            FileOutputStream fileOut
                    = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(m);
            out.close();
            fileOut.close();

        } catch (IOException i) {
            //Error handling
            i.printStackTrace();
        }

        //Make another folder in rentals for the user
        path = "./resources/rentals/" + memNumber + "/";
        File folder = new File(path);
        folder.mkdir();

    }

    public boolean memberExists(String memNumber) {
        //Check if member file exists
        String path = "./resources/members/" + memNumber + ".ser";
        File file = new File(path);
        if (file.exists()) {
            return true;
        } else {
            return false;
        }
    }

    public void upgradeMember(String memNumber) {
        //Open member file
        String path = "./resources/members/" + memNumber + ".ser";
        File f = new File(path);
        Member m;
        try {
            //Retrieve Member object into m
            FileInputStream fileIn = new FileInputStream(f);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            m = (Member) in.readObject();
            in.close();
            fileIn.close();

            //Upgrade status (Member gets changed to P for all cases);
            m.setType("P");

            //Store the file
            FileOutputStream fileOut
                    = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(m);
            out.close();
            fileOut.close();

            //Required output
            System.out.println("\nUpgraded Member Details: Name: "
                    + m.getName() + ", Member Number: " + m.getMemNumber()
                    + ", Member Type: " + m.getType() + ", Address: "
                    + m.getAdd() + ", Contact No: " + m.getContactNo()
                    + ", Max Points: " + m.getMaxPoints());

        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("Member class not found");
            c.printStackTrace();
        }

    }

    public void cancelMember(String memNumber) {
        //Check if rental folder is empty
        String path = "./resources/rentals/" + memNumber + "/";
        File folder = new File(path);
        if (folder.list().length > 0) {
            System.out.println("Member has outstanding items due.");
            return;
        }
        //Delete the members folder
        path = "./resources/members/" + memNumber + ".ser";
        File file = new File(path);
        file.delete();

    }

    public void displayAllMembers() {
        //Members directory
        String dir = "./resources/members/";
        File file = new File(dir);

        //Deserialise every members file
        File[] allMems = file.listFiles();
        Member m = null;
        if (allMems != null) {
            for (File f : allMems) {
                //Convert f to object
                try {
                    FileInputStream fileIn = new FileInputStream(f);
                    ObjectInputStream in = new ObjectInputStream(fileIn);
                    m = (Member) in.readObject();

                    //Print out relevant information
                    System.out.println("Name:" + m.getName() + ", "
                            + "Member Number " + m.getMemNumber()
                            + ", Member Type: " + m.getType() + ", Address: "
                            + m.getAdd() + ", Contact No: " + m.getContactNo()
                            + ", Max Points: " + m.getMaxPoints());

                    in.close();
                    fileIn.close();
                } catch (IOException i) {
                    i.printStackTrace();
                } catch (ClassNotFoundException c) {
                    System.out.println("Movie class not found");
                    c.printStackTrace();
                }
            }
        }
    }

    public void createPayment(double amtPaid, Date Paid, int Type) {

    }

    public String dateToString(Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        String reportDate = df.format(date);

        return reportDate;
    }

    public void displayMember(String memNumber) {
        //First we check the copies associated with this member
        String path = "./resources/rentals/" + memNumber + "/";
        File file = new File(path);
        File[] allMems = file.listFiles();
        Rental r = null;
        Movie m = null;
        Copy cp = null;
        if (allMems != null) {

            //Exceptional case
            if (allMems.length == 0) {
                System.out.println("\nMember has no rented items.\n");
            }
            for (File f : allMems) {
                //Convert f to object
                try {
                    FileInputStream fileIn = new FileInputStream(f);
                    ObjectInputStream in = new ObjectInputStream(fileIn);
                    r = (Rental) in.readObject();

                    String rentDate = dateToString(r.getDateRented());
                    String returnDate = dateToString(r.getDateToReturn());
                    String title = r.getTitle();
                    String cID = r.getId();

                    //Print out relevant information of Movie
                    //Open up the movie object
                    path = "./resources/movies/" + title + ".ser";
                    m = getMovieFromPath(path);
                    String director = m.getDirector();
                    int year = m.getYear();
                    String rating = m.getRating();
                    String genre = m.getGenre();
                    String id = getMovieID(title);
                    System.out.println("Movie Details:\n"
                            + "Date Rented: " + rentDate + ", Date to Return: "
                            + returnDate + ", Movie ID: " + id + ", Title: "
                            + title + ", Year: " + year + ", Director: "
                            + director + ", Rating: " + rating + ", Genre: "
                            + genre + "\n");
                    //Print out relevant information of Copy
                    //Open up the copy object
                    path = "./resources/copies/" + title + "/" + cID + ".ser";
                    cp = getCopyFromPath(path);
                    Double cost = cp.getCost();
                    String format = cp.getFormat();
                    String dateCreated = dateToString(cp.getDateCreated());
                    System.out.println("Copy Details\n"
                            + "Copy ID: " + cID + ", Cost: " + cost
                            + ", Format: " + format + ", Date Created: "
                            + dateCreated + "\n");

                    in.close();
                    fileIn.close();
                } catch (IOException i) {
                    i.printStackTrace();
                } catch (ClassNotFoundException c) {
                    System.out.println("Class not found");
                    c.printStackTrace();
                }
            }
        }

    }

    public void displaySearchResults(String searchTerm, String method) {
        //Iterate through movies directory
        String dir = "./resources/movies/";
        File file = new File(dir);
        File[] allMovies = file.listFiles();
        Movie m = null;
        ArrayList list = new ArrayList();
        if (allMovies != null) {
            for (File f : allMovies) {
                //Convert f to object
                try {
                    FileInputStream fileIn = new FileInputStream(f);
                    ObjectInputStream in = new ObjectInputStream(fileIn);
                    m = (Movie) in.readObject();
                    in.close();
                    fileIn.close();

                    //Add movie title to array if matching
                    if (method.equals("year")) {
                        if (searchTerm.equals(Integer.toString(m.getYear()))) {
                            list.add(m.getTitle());
                        }
                    } else if (method.equals("director")) {
                        if (searchTerm.equals(m.getDirector())) {
                            list.add(m.getTitle());
                        }
                    } else if (method.equals("rating")) {
                        if (searchTerm.equals(m.getRating())) {
                            list.add(m.getTitle());
                        }
                    } else if (method.equals("rating")) {
                        if (searchTerm.equals(m.getGenre())) {
                            list.add(m.getTitle());
                        }
                    }
                } catch (IOException i) {
                    i.printStackTrace();
                } catch (ClassNotFoundException c) {
                    System.out.println("Movie class not found");
                    c.printStackTrace();
                }
            }
        }

        if (list.isEmpty()) {
            System.out.println("No results found");
        } else {
            for (int i = 0; i < list.size(); i++) {
                displayMovieDetails((String) list.get(i));
            }
        }

    }

    public void displayMovieDetails(String title) {
        Movie m = getMovieFromTitle(title);
        if (m != null) {
            System.out.println("ID: " + getMovieID(title) + ", Title: "
                    + m.getTitle() + ", Year: " + m.getYear() + ", Director: "
                    + m.getDirector() + ", Rating: " + m.getRating()
                    + ", Genre: " + m.getGenre() + ", Total Available Copies: "
                    + getAvailableCopies(title));
        } else {
            System.out.println("No results found");
        }
    }

    public Movie getMovieFromTitle(String title) {
        String path = "./resources/movies/" + title + ".ser";
        return getMovieFromPath(path);
    }

    public Movie getMovieFromPath(String path) {
        Movie m = null;
        try {
            File mFile = new File(path);
            FileInputStream mFileIn = new FileInputStream(mFile);
            ObjectInputStream mIn = new ObjectInputStream(mFileIn);
            m = (Movie) mIn.readObject();
            mIn.close();
            mFileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("Movie Class not found");
            c.printStackTrace();
        }

        return m;
    }

    public double getCostOfRent(String memNumber, String format) {
        Member mem = getMemberFromNumber(memNumber);

        if (format.equals("DVD")) {
            return mem.getDVDrentFee();
        } else if (format.equals("VCD")) {
            return mem.getVCDrentFee();
        } else {
            return 0.00;
        }
    }

    public Member getMemberFromNumber(String memNumber) {
        Member m = null;
        String path = "./resources/members/" + memNumber + ".ser";
        try {
            File mFile = new File(path);
            FileInputStream mFileIn = new FileInputStream(mFile);
            ObjectInputStream mIn = new ObjectInputStream(mFileIn);
            m = (Member) mIn.readObject();
            mIn.close();
            mFileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("Member Class not found");
            c.printStackTrace();
        }

        return m;
    }

    public Copy getCopyFromPath(String path) {
        Copy cp = null;
        try {
            File mFile = new File(path);
            FileInputStream mFileIn = new FileInputStream(mFile);
            ObjectInputStream mIn = new ObjectInputStream(mFileIn);
            cp = (Copy) mIn.readObject();
            mIn.close();
            mFileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("Movie Class not found");
            c.printStackTrace();
        }

        return cp;
    }

    public Movie getMovie() {
        Movie m = new Movie();
        return m;
    }

    public Copy getCopy() {
        Copy c = new Copy();
        return c;
    }

    public Rental getRental() {
        Rental r = new Rental();
        return r;

    }

    public Privilege getPrivilege(String type) {
        Privilege p = new Privilege(type);
        return p;
    }

    public Member getMember() {
        Member m = new Member();
        return m;
    }

    public Payment getPayment() {
        Payment pmt = new Payment();
        return pmt;
    }

}
