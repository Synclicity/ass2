
package ass2;

public class Movie implements java.io.Serializable {

    private String title;
    private int year;
    private String director;
    private String rating;
    private String genre;

    public Movie(String new_title, int new_year, String new_director,
            String new_rating, String new_genre) {
        title = new_title;
        year = new_year;
        director = new_director;
        rating = new_rating;
        genre = new_genre;
    }
    
    public Movie() {
        
    }

    public void checkAvailableCopy() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getGenre() {
        return genre.toUpperCase();
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    

}
